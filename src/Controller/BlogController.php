<?php


namespace App\Controller;

use App\Service\GreetingService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @var GreetingService
     */
    private $greetingService;

    /**
     * BlogController constructor.
     * @param GreetingService $greetingService
     */
    public function __construct(GreetingService $greetingService)
    {
        $this->greetingService = $greetingService;
    }

    /**
     * @Route("/greet", name="get_greet", methods={"GET"})
     * @param Request $request
     * @return mixed
     */
    public function list(Request $request)
    {
        return $this->render('base.html.twig', [
            'message' => $this->greetingService->greet($request->get('name'))
        ]);
    }
}